<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Activity tracker</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<script>
    let wrapper = document.createElement('div');
    wrapper.id = 'wrapper';

    let header = document.createElement('div');
    header.id = 'header';
    header.innerHTML = 'Activity tracker';
    
    wrapper.append(header);

    let divform = document.createElement('div');
    divform.id = 'form';

    let form = document.createElement('form');
    form.action = 'index.php';
    form.method = 'POST';
    form.className = 'form';

    let textForm = document.createElement('span');
    textForm.innerHTML = 'Add new activity:';
    textForm.className = 'textForm';
    
    form.append(textForm);

    let inputStartTime = document.createElement('input');
    inputStartTime.type = 'datetime-local';
    inputStartTime.id = 'startTime';
    inputStartTime.name = 'startTime';
    inputStartTime.placeholder = 'Start time';
    
    form.append(inputStartTime);

    let inputFinishTime = document.createElement('input');
    inputFinishTime.type = 'datetime-local';
    inputFinishTime.id = 'finishTime';
    inputFinishTime.name = 'finishTime';
    inputFinishTime.placeholder = 'Finish time';
    
    form.append(inputFinishTime);

    let distance = document.createElement('input');
    distance.type = 'number';
    distance.id = 'distance';
    distance.step = 'any';
    distance.name = 'distance';
    distance.min = '0';
    distance.placeholder = 'Distance';
    
    form.append(distance);

    let selectActivity = document.createElement('select');
    selectActivity.name = 'activity';

    let optionActivityType = document.createElement('option');
    optionActivityType.innerHTML = 'Select activity type';
    optionActivityType.disabled = 'disabled';
    optionActivityType.selected = 'selected';
    selectActivity.append(optionActivityType);

    let optionActivityRide = document.createElement('option');
    optionActivityRide.value = 'Ride';
    optionActivityRide.innerHTML = 'Ride';
    selectActivity.append(optionActivityRide);

    let optionActivityRun = document.createElement('option');
    optionActivityRun.value = 'Run';
    optionActivityRun.innerHTML = 'Run';
    selectActivity.append(optionActivityRun);

    form.append(selectActivity);

    let buttonActivitySave = document.createElement('input');
    buttonActivitySave.type = 'submit';
    buttonActivitySave.id = 'save';
    buttonActivitySave.name = 'save';
    buttonActivitySave.value = 'Save';

    form.append(buttonActivitySave);

    divform.append(form);
    wrapper.append(divform);
    document.body.append(wrapper);

    document.getElementById('distance').onkeydown = function (e) {
        if (e.currentTarget.value.indexOf(".") != '-1' || e.currentTarget.value.indexOf(",") != '-1') { 
        return !(/[.]/.test(e.key));
        }
    }
</script>
<?php include('constants.php'); ?>
<?php include('formProcessing.php'); ?>
</body>
</html>