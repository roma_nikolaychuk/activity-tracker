-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Час створення: Лют 06 2021 р., 14:28
-- Версія сервера: 10.3.22-MariaDB
-- Версія PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `activitytracker`
--

-- --------------------------------------------------------

--
-- Структура таблиці `activitytracker`
--

CREATE TABLE `activitytracker` (
  `id` int(10) UNSIGNED NOT NULL,
  `dateOfActivity` varchar(25) NOT NULL,
  `typeOfActivity` varchar(25) NOT NULL,
  `distance` double NOT NULL,
  `duration` varchar(25) NOT NULL,
  `averageSpeed` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `activitytracker`
--

INSERT INTO `activitytracker` (`id`, `dateOfActivity`, `typeOfActivity`, `distance`, `duration`, `averageSpeed`) VALUES
(1, 'December 11', 'Run', 3.4, '35 minutes', '5.8 km/hour'),
(11, 'October 23', 'Ride', 25, '1 h 45 m', '14.3 km/hour'),
(12, 'October 15', 'Run', 3.4, '35 minutes', '5.8 km/hour'),
(13, 'September 30', 'Run', 5, '30 minutes', '10 km/hour'),
(14, 'September 25', 'Run', 3.4, '35 minutes', '5.8 km/hour'),
(15, 'September 22', 'Ride', 10.2, '28 minutes', '21.9 km/hour'),
(16, 'February 6', 'Run', 5.5, '1 h 10 m', '4.7 km/hour'),
(17, 'February 6', 'Ride', 75, '1 hour', '75 km/hour'),
(21, 'February 8', 'Run', 7, '1 hour', '7 km/hour'),
(25, 'February 10', 'Ride', 120.5, '1 h 30 m', '80.3 km/hour'),
(26, 'February 10', 'Ride', 100, '2 hour', '50 km/hour');
--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `activitytracker`
--
ALTER TABLE `activitytracker`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `activitytracker`
--
ALTER TABLE `activitytracker`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
