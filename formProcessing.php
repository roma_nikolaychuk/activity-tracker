<?php
    if(isset($_POST['save']))
    {
        if(!empty($_POST["startTime"]) and !empty($_POST["finishTime"]) and !empty($_POST["distance"]) and !empty($_POST["activity"]))
        {
            $inputStartTime = $_POST['startTime'];
            $dateStart = explode('T', $inputStartTime);
            $dateStartYearMonthDay = explode('-', $dateStart[0]);
            $timeStartHoursMinutes = explode(':', $dateStart[1]);
            
            $strDate = strtotime($dateStart[0]);
            $textDate = date('F j', $strDate);

            $inputFinishTime = $_POST['finishTime'];
            $dateFinish = explode('T', $inputFinishTime);
            $dateFinishYearMonthDay = explode('-', $dateFinish[0]);
            $timeFinishHoursMinutes = explode(':', $dateFinish[1]);

            $startTimeInMinutes = $timeStartHoursMinutes[0] * 60 + $timeStartHoursMinutes[1];
            $finishTimeInMinutes = $timeFinishHoursMinutes[0] * 60 + $timeFinishHoursMinutes[1];

            $duration = $finishTimeInMinutes - $startTimeInMinutes;

            $textDuration = '';

            if($duration > 0)
            {
                if($duration % 60 == 0)
                {
                    $textDuration = intval($duration / 60).' hour';
                } elseif ($duration < 60) {
                    $textDuration = $duration.' minutes';
                } else {
                    $textDuration = intval($duration / 60).' h '.($duration % 60).' m';
                }

                $distance = $_POST['distance'];
                $averageSpeed = round($distance / ($duration / 60), 1).' km/hour';
            } else {
                $textDuration = '0 minutes';
                $averageSpeed = '0 km/hour';
            }

            $activity = $_POST["activity"];
        
            $sql1 = "INSERT INTO activitytracker SET 
                dateOfActivity='$textDate',
                typeOfActivity='$activity',
                distance='$distance',
                duration='$textDuration',
                averageSpeed='$averageSpeed'
            ";

            $res1 = mysqli_query($conn, $sql1) or die(mysqli_error());
        
        } else {
            echo '<script>alert("Заповніть усі поля!");</script>';
        }
    }

    $sql2 = "SELECT * FROM activitytracker";
    $res2 = mysqli_query($conn, $sql2);
    

    $sql3 = "SELECT dateOfActivity, distance, duration FROM activitytracker WHERE distance=(SELECT MAX(distance) FROM activitytracker WHERE typeOfActivity='Ride')";
    $res3 = mysqli_query($conn, $sql3);
    

    $sql4 = "SELECT dateOfActivity, distance, duration FROM activitytracker WHERE distance=(SELECT MAX(distance) FROM activitytracker WHERE typeOfActivity='Run')";
    $res4 = mysqli_query($conn, $sql4);
    

    $sql5 = "SELECT SUM(distance) AS sumDistanceRide FROM activitytracker WHERE typeOfActivity='Ride'";
    $res5 = mysqli_query($conn, $sql5);

    $sql6 = "SELECT SUM(distance) AS sumDistanceRun FROM activitytracker WHERE typeOfActivity='Run'";
    $res6 = mysqli_query($conn, $sql6);
    $count = mysqli_num_rows($res6);

    if($count > 0)
    {
        echo '<div id="resultsTracker">';
            echo '<div id="resultsTrackerAdd">';
                while($row=mysqli_fetch_assoc($res2))
                {
                    echo '<div class="result">
                        <p>'.$row['dateOfActivity'].'</p>
                        <p>'.$row['typeOfActivity'].'</p>
                        <p>'.$row['distance'].' km'.'</p>
                        <p>'.$row['duration'].'</p>
                        <p>'.$row['averageSpeed'].'</p>
                    </div>';
                }
            echo '</div>';

        echo '<div id="recordResultTracker">';
            echo '<div id="recordResultTrackerLongest">';
                while($row=mysqli_fetch_assoc($res3))
                {
                    echo '<p class="textBold">Longest ride:</p>
                        <p class="longestRecord">'.$row['dateOfActivity'].'</p>
                        <p class="longestRecord">'.$row['distance'].' km'.'</p>
                        <p class="longestRecord">'.$row['duration'].'</p>';
                }

                while($row=mysqli_fetch_assoc($res4))
                {
                    echo '<p class="textBold">Longest run:</p>
                        <p class="longestRecord">'.$row['dateOfActivity'].'</p>
                        <p class="longestRecord">'.$row['distance'].' km'.'</p>
                        <p class="longestRecord">'.$row['duration'].'</p>';
                }
            echo '</div>';

            echo '<div id="recordResultTrackerTotal">';
                while($row=mysqli_fetch_assoc($res5))
                {
                    echo '<p class="textBold">Total ride distance: <span>'.round($row['sumDistanceRide'], 1).' km</span></p>';
                }

                while($row=mysqli_fetch_assoc($res6))
                {
                    echo '<p class="textBold">Total run distance: <span>'.round($row['sumDistanceRun'], 1).' km</span></p>';
                }
            echo '</div>';
            
            echo '</div>';
        echo '</div>';
    }
?>
